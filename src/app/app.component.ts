import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { Subject, switchMap } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  numberName$ = this.appService.numberName$;
  private numberLookup$: Subject<void> = new Subject();

  form = new UntypedFormGroup({
    number: new UntypedFormControl(''),
  });

  formNumber = this.form.get('number') as UntypedFormControl;

  constructor(protected appService: AppService) {}

  ngOnInit(): void {
    this.numberLookup$
      .pipe(
        switchMap(() => {
          return this.appService.getNumberName(this.formNumber.value);
        })
      )
      .subscribe();

    this.form.valueChanges.subscribe(() => {
      let number = Number(this.formNumber.value);
      if (number) {
        this.numberLookup$.next();
      }
    });
  }
}
