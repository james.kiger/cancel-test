import { map, tap } from "rxjs";
import { StatefulService } from "./shared/stateful-service";
import { APIService } from "./api/api-service";
import { Injectable } from "@angular/core";

export interface AppState {
  numberName: string
}

const initialState: AppState = {
  numberName: ""
}

@Injectable({
  providedIn: 'root',
})
export class AppService extends StatefulService<AppState> {
  numberName$ = this.getState$.pipe(map((state) => state.numberName));

  constructor(private ApiService: APIService) {
    super(initialState);
  }

  getNumberName(number: number) {
    return this.retrieveNumberName(number);
  }

  private retrieveNumberName(number: number) {
    return this.ApiService.get(number).pipe(
      tap((res) => {
        this.setState({ numberName: res });
      })
    );
  }
}